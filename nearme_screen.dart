import 'dart:html';

import 'package:flutter/material.dart';

class NearMeScreen extends StatefulWidget {
  const NearMeScreen({Key? key}) : super(key: key);

  @override
  _NearMeScreenState createState() => _NearMeScreenState();
}

class _NearMeScreenState extends State<NearMeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Near Me"),
      ),
    );
  }
}
